﻿using System;
using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
#else
using Microsoft.Framework.DependencyInjection;
#endif
using LendFoundry.DataAttributes.Client;
using LendFoundry.EventHub;
using LendFoundry.Clients.DecisionEngine;
using LendFoundry.Email.Client;
using LendFoundry.Sms.Client;

namespace CreditExchange.EventDrivenNotifications
{
    public class EventDrivenNotificationServiceFactory : IEventDrivenNotificationServiceFactory
    {
        public EventDrivenNotificationServiceFactory(IServiceProvider provider)
        {
            Provider = provider;
        }
        public IServiceProvider Provider { get; }

        public IEventDrivenNotificationService Create(ITokenReader reader, ILogger logger)
        {
            var emailServiceFactory = Provider.GetService<IEmailServiceFactory>();
            var emailService = emailServiceFactory.Create(reader);

            var smsServiceFactory = Provider.GetService<ISmsServiceFactory>();
            var smsService = smsServiceFactory.Create(reader);

            var eventHubFactory = Provider.GetService<IEventHubClientFactory>();
            var eventHub = eventHubFactory.Create(reader);

            var decisionEngineServiceFactory = Provider.GetService<IDecisionEngineClientFactory>();
            var decisionEngine = decisionEngineServiceFactory.Create(reader);

            var dataAttributeClientFactory = Provider.GetService<IDataAttributesClientFactory>();
            var dataAttributesEngine = dataAttributeClientFactory.Create(reader);

            return new EventDrivenNotificationService(logger, emailService, smsService, eventHub, decisionEngine, dataAttributesEngine);
        }
    }
}
