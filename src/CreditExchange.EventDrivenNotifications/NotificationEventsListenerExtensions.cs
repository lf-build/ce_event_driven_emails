﻿#if DOTNET2
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
#else
using Microsoft.AspNet.Builder;
using Microsoft.Framework.DependencyInjection;
#endif

namespace CreditExchange.EventDrivenNotifications
{
    public static class EmailEventsListenerExtensions
    {
        public static void UseNotificationEventsListener(this IApplicationBuilder app)
        {
            app.ApplicationServices.GetRequiredService<INotificationEventsListener>().Start();
        }
    }
}