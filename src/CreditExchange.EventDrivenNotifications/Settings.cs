﻿using LendFoundry.Foundation.Services.Settings;
using System;

namespace CreditExchange.EventDrivenNotifications
{
    public static class Settings
    {
        public static string ServiceName { get; } = "event-driven-notifications";
    }
}
