﻿using System;
using LendFoundry.EventHub;
using CreditExchange.EventDrivenNotifications.Configuration;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using LendFoundry.Email;
using LendFoundry.Sms;
using LendFoundry.Clients.DecisionEngine;
using LendFoundry.DataAttributes;

namespace CreditExchange.EventDrivenNotifications
{
    public class EventDrivenNotificationService : IEventDrivenNotificationService
    {
        public EventDrivenNotificationService(ILogger logger,
                                              IEmailService emailService,
                                              ISmsService smsService,
                                              IEventHubClient eventHub,
                                              IDecisionEngineService decisionEngine,
                                              IDataAttributesEngine dataAttributesEngine)
        {
            Logger = logger;
            EmailService = emailService;
            SmsService = smsService;
            Eventhub = eventHub;
            DecisionEngine = decisionEngine;
            DataAttributesEngine = dataAttributesEngine;
        }

        private ILogger Logger { get; }
        private IEventHubClient Eventhub { get; }
        private IEmailService EmailService { get; }
        private ISmsService SmsService { get; }
        private IDecisionEngineService DecisionEngine { get; }
        private IDataAttributesEngine DataAttributesEngine { get; }

        public Action<EventInfo> ProcessNotificationEvent(NotificationEventsConfiguration configuration)
        {
            return async @event =>
            {
                try
                {
                    Logger.Info($"Now processing event : {@event.Name}");
                        if (configuration == null)
                        throw new ArgumentException($"The configuration for service #{Settings.ServiceName} could not be found, please verify");

                    var notificationType = WhatNotificationToSend(configuration);
                    var entityId = configuration.EntityId.FormatWith(@event);

                    var dataAttributes = await DataAttributesEngine.GetAllAttributes("application", entityId);
                    var payload = new { EventData = @event.Data, DataAttributes = dataAttributes };

                    ISendEmailResponse emailResult = null;
                    SmsResult mobileResult = null;

                    switch (notificationType)
                    {
                        case NotificationType.Email:
                            var emailTemplateData = GetTemplateDataForEvent(configuration.EmailTemplate.DataExtractionRule.Name, configuration.EmailTemplate.DataExtractionRule.Version, payload);
                            if (emailTemplateData == null)
                            {
                                throw new Exception($"Data missing for populating template {configuration.EmailTemplate.TemplateName} for entity {entityId}");
                            }
                            emailResult = await EmailService.Send("application", entityId, configuration.EmailTemplate.TemplateName, configuration.EmailTemplate.TemplateVersion, emailTemplateData, null);
                            break;
                        case NotificationType.Mobile:
                            var mobileTemplateData = GetTemplateDataForEvent(configuration.MobileTemplate.DataExtractionRule.Name, configuration.MobileTemplate.DataExtractionRule.Version, payload);
                            if (mobileTemplateData == null)
                            {
                                throw new Exception($"Data missing for populating template {configuration.MobileTemplate.TemplateName} for entity {entityId}");
                            }
                            if (!string.IsNullOrEmpty(CheckPhoneValue(mobileTemplateData)))
                                mobileResult = await SmsService.Send("application", entityId, configuration.MobileTemplate.TemplateName, configuration.MobileTemplate.TemplateVersion, mobileTemplateData);
                            else
                                Logger.Info("RecipientPhone Property or Value is not found while executing rule :" + configuration.MobileTemplate.DataExtractionRule.Name + "for entityId" + entityId);
                            break;
                        case NotificationType.Both:
                            var emailTemplateDataForBoth = GetTemplateDataForEvent(configuration.EmailTemplate.DataExtractionRule.Name, configuration.EmailTemplate.DataExtractionRule.Version, payload);
                            if (emailTemplateDataForBoth != null && !string.IsNullOrEmpty(CheckEmailValue(emailTemplateDataForBoth)))
                            {
                                emailResult = await EmailService.Send("application", entityId, configuration.EmailTemplate.TemplateName, configuration.EmailTemplate.TemplateVersion, emailTemplateDataForBoth, null);
                            }
                            var mobileTemplateDataBoth = GetTemplateDataForEvent(configuration.MobileTemplate.DataExtractionRule.Name, configuration.MobileTemplate.DataExtractionRule.Version, payload);
                            if (mobileTemplateDataBoth == null)
                            {
                                throw new Exception($"Data missing for populating template {configuration.MobileTemplate.TemplateName} for entity {entityId}");
                            }
                            mobileResult = await SmsService.Send("application", entityId, configuration.MobileTemplate.TemplateName, configuration.MobileTemplate.TemplateVersion, mobileTemplateDataBoth);
                            break;
                    }

                    if (emailResult != null)
                    {
                        await Eventhub.Publish($"{configuration.EventName}EmailProcessed", new
                        {
                            EntityType = "application",
                            EntityId = entityId,
                            Title = $"{configuration.EventName}EmailProcessed",
                            Description = $"{configuration.EventName}EmailProcessed"
                        });
                    }

                    if (mobileResult != null)
                    {
                        await Eventhub.Publish($"{configuration.EventName}SmsProcessed", new
                        {
                            EntityType = "application",
                            EntityId = entityId,
                            Title = $"{configuration.EventName}SmsProcessed",
                            Description = $"{configuration.EventName}SmsProcessed"
                        });
                    }

                }
                catch (Exception ex)
                {
                    Logger.Error($"Error while processing event #{@event.Name}. Error: {ex.Message}", ex);
                    Logger.Info("\nEvent Driven Notification is working yet and waiting new event\n");
                }
            };
        }

        private NotificationType WhatNotificationToSend(NotificationEventsConfiguration configuration)
        {
            bool IsEmailNotificationConfigured = false;
            bool IsMobileNotificationConfigured = false;

            if (configuration.EmailTemplate != null)
            {
                IsEmailNotificationConfigured = true;
            }

            if (configuration.MobileTemplate != null)
            {
                IsMobileNotificationConfigured = true;
            }

            if (IsEmailNotificationConfigured && IsMobileNotificationConfigured)
            {
                return NotificationType.Both;
            }
            else if (IsEmailNotificationConfigured)
            {
                return NotificationType.Email;
            }
            else if (IsMobileNotificationConfigured)
            {
                return NotificationType.Mobile;
            }
            else
            {
                throw new Exception($"Notification types not configured for event {configuration.EventName}");
            }
        }

        private object GetTemplateDataForEvent(string ruleName, string ruleVersion, object payload)
        {
            object ruleResult = null;
            try
            {
                ruleResult = DecisionEngine.Execute<dynamic, object>(ruleName, new { input = payload });
                if (ruleResult == null)
                {
                    throw new Exception($"Could not extract data for template using rule {ruleName}.");
                }
            }
            catch (Exception ex)
            {
                Logger.Error($"Failure while fetching data for template using rule {ruleName}.", ex);
            }
            return ruleResult;
        }

        private static string CheckPhoneValue(dynamic data)
        {
            string recipientPhone = string.Empty;
            Func<dynamic> phoneProperty = () => data.RecipientPhone;
            if (HasProperty(phoneProperty))            
                recipientPhone = GetValue(phoneProperty);
            return recipientPhone;
        }

        private static string CheckEmailValue(dynamic data)
        {
            string emailValue = string.Empty;
            Func<dynamic> emailProperty = () => data.Email;
            if (HasProperty(emailProperty))
                emailValue = GetValue(emailProperty);
            return emailValue;
        }
        private static bool HasProperty<T>(Func<T> @property)
        {
            try
            {
                @property();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        private static T GetValue<T>(Func<T> @property)
        {
            return @property();
        }

    }
}