﻿using CreditExchange.EventDrivenNotifications.Configuration;
using LendFoundry.Configuration;
using LendFoundry.EventHub;
using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using System;
using System.Linq;

namespace CreditExchange.EventDrivenNotifications
{
    public class NotificationEventsListener : INotificationEventsListener
    {
        public NotificationEventsListener
        (
            IConfigurationServiceFactory configurationFactory,
            ITokenHandler tokenHandler,
            IEventHubClientFactory eventHubFactory,
            ILoggerFactory loggerFactory,
            ITenantServiceFactory tenantServiceFactory,
            IEventDrivenNotificationServiceFactory eventDrivenNotificationServiceFactory
        )
        {           
            ConfigurationFactory = configurationFactory;
            TokenHandler = tokenHandler;
            EventHubFactory = eventHubFactory;
            LoggerFactory = loggerFactory;
            TenantServiceFactory = tenantServiceFactory;
            EventDrivernNotificationServiceFactory = eventDrivenNotificationServiceFactory;
        }
        
        private IConfigurationServiceFactory ConfigurationFactory { get; }

        private IEventHubClientFactory EventHubFactory { get; }

        private ITenantServiceFactory TenantServiceFactory { get; }

        private ITokenHandler TokenHandler { get; }

        private ILoggerFactory LoggerFactory { get; }
                
        private IEventDrivenNotificationServiceFactory EventDrivernNotificationServiceFactory { get; }

        public void Start()
        {
            var logger = LoggerFactory.Create(NullLogContext.Instance);
            logger.Info("Starting Listener...");

            try
            {
                var emptyReader = new StaticTokenReader(string.Empty);                
                var tenantService = TenantServiceFactory.Create(emptyReader);
                var tenants = tenantService.GetActiveTenants();
                logger.Info($"#{tenants.Count} active tenant(s) found to be processed");

                // Get all active tenants in place and set an eventhub subscription for each one.
                tenants.ForEach(tenant =>
                {
                    logger.Info($"Processing tenant #{tenant.Id}");
                    var token = TokenHandler.Issue(tenant.Id, Settings.ServiceName);
                    var reader = new StaticTokenReader(token.Value);
                    var eventhub = EventHubFactory.Create(reader);

                    var configuration = ConfigurationFactory.Create<EventDrivenNotificationConfiguration>(Settings.ServiceName, reader).Get();

                    if (configuration == null)
                    {
                        logger.Error($"The configuration for service #{Settings.ServiceName} could not be found, please verify");
                    }
                    else
                    {
                        logger.Info($"#{configuration.EmailEvents.Count()} entity(ies) found from configuration: {Settings.ServiceName}");
                        logger.Info("-------------------------------------------");

                        var EventDrivenEmailsService = EventDrivernNotificationServiceFactory.Create(reader, logger);

                        configuration
                            .EmailEvents
                            .ToList()
                            .ForEach(eventConfig => eventhub.On(eventConfig.EventName, EventDrivenEmailsService.ProcessNotificationEvent(eventConfig)));

                        logger.Info("-------------------------------------------");
                        eventhub.StartAsync();
                    }
                });                
                logger.Info("Event Driven Email listener started");
            }            
            catch (Exception ex)
            {
                Start();
                logger.Error("Error while listening eventhub to process Events for email", ex);
                logger.Info("\nEvent Driven Email is working yet and waiting new event\n");
            }
        }
    }
}