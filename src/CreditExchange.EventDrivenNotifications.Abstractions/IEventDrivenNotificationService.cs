﻿using System;
using LendFoundry.EventHub;
using CreditExchange.EventDrivenNotifications.Configuration;
using LendFoundry.Security.Tokens;

namespace CreditExchange.EventDrivenNotifications
{
    public interface IEventDrivenNotificationService
    {
        Action<EventInfo> ProcessNotificationEvent(NotificationEventsConfiguration configuration);
    }
}
