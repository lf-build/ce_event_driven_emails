﻿namespace CreditExchange.EventDrivenNotifications
{
    public interface INotificationEventsListener
    {
        void Start();
    }
}