﻿namespace CreditExchange.EventDrivenNotifications
{
    public enum NotificationType
    {
        Email,
        Mobile,
        Both
    }
}
