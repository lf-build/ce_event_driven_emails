﻿namespace CreditExchange.EventDrivenNotifications.Configuration
{
    public class NotificationEventsConfiguration
    {
        public string EventName { get; set; }
        public string EntityId { get; set; }        
        public TemplateConfig EmailTemplate { get; set; }
        public TemplateConfig MobileTemplate { get; set; }
    }
}
