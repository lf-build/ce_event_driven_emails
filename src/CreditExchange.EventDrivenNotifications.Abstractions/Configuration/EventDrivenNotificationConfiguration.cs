﻿using System.Collections.Generic;

namespace CreditExchange.EventDrivenNotifications.Configuration
{
    public class EventDrivenNotificationConfiguration : IEventDrivenNotificationConfiguration
    {
        public IEnumerable<NotificationEventsConfiguration> EmailEvents { get; set; }
    }
}
