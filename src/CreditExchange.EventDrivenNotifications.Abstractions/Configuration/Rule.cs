﻿using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CreditExchange.EventDrivenNotifications.Configuration
{
    public class Rule
    {
        public string Name { get; set; }
        public string Version { get; set; }
    }
}
