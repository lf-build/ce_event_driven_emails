﻿using System.Collections.Generic;

namespace CreditExchange.EventDrivenNotifications.Configuration
{
    public interface IEventDrivenNotificationConfiguration
    {
        IEnumerable<NotificationEventsConfiguration> EmailEvents { get; set; }
    }
}
