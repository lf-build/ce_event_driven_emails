﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CreditExchange.EventDrivenNotifications.Configuration
{
    public class TemplateConfig
    {
        public string TemplateName { get; set; }
        public string TemplateVersion { get; set; }
        public Rule DataExtractionRule { get; set; }
    }
}
