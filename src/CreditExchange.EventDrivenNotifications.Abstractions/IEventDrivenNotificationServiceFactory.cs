﻿using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;

namespace CreditExchange.EventDrivenNotifications
{
    public interface IEventDrivenNotificationServiceFactory
    {
        IEventDrivenNotificationService Create(ITokenReader reader, ILogger logger);
    }
}
