﻿using LendFoundry.Foundation.Services;
#if DOTNET2
using Microsoft.AspNetCore.Mvc;
#else
using Microsoft.AspNet.Mvc;
#endif

namespace CreditExchange.EventDrivenNotifications.Api.Controllers
{
    [Route("/")]
    public class EventDrivenEmailController : ExtendedController
    {
        
    }
}
