﻿using LendFoundry.Foundation.Services.Settings;
using System;

namespace CreditExchange.EventDrivenNotifications.Api
{
    public static class Settings
    {
        public static string ServiceName { get; } = "event-driven-notifications";

        private static string Prefix { get; } = ServiceName.ToUpper();

        public static ServiceSettings Configuration { get; } = new ServiceSettings($"{Prefix}_CONFIGURATION", "configuration");

        public static ServiceSettings EventHub { get; } = new ServiceSettings($"{Prefix}_EVENTHUB", "eventhub");

        public static ServiceSettings Tenant { get; } = new ServiceSettings($"{Prefix}_TENANT", "tenant");
        
        public static ServiceSettings Email { get; } = new ServiceSettings($"{Prefix}_EMAIL", "ce-email");

        public static ServiceSettings Sms { get; } = new ServiceSettings($"{Prefix}_SMS", "sms");

        public static ServiceSettings DataAttributes { get; } = new ServiceSettings($"{Prefix}_DATA_ATTRIBUTES", "data-attributes");

        public static ServiceSettings LookupService { get; } = new ServiceSettings($"{Prefix}_LOOKUPSERVICE","lookup-service");

        public static ServiceSettings DecisionEngine { get; } = new ServiceSettings($"{Prefix}_DECISION_ENGINE_RUNTIME", "decision-engine");

        public static string Nats => Environment.GetEnvironmentVariable($"{Prefix}_NATS_URL") ?? "nats";
    }
}
