﻿using LendFoundry.Configuration.Client;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
#if DOTNET2
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.PlatformAbstractions;
using Swashbuckle.AspNetCore.Swagger;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.AspNetCore.Http;
using System.IO;
#else
using Microsoft.AspNet.Builder;
using Microsoft.AspNet.Hosting;
using Microsoft.Framework.DependencyInjection;
using LendFoundry.Foundation.Documentation;
#endif
using CreditExchange.EventDrivenNotifications.Configuration;
using LendFoundry.DataAttributes.Client;
using LendFoundry.Foundation.Lookup.Client;
using LendFoundry.Clients.DecisionEngine;
using LendFoundry.Email.Client;
using LendFoundry.Sms.Client;
using System;

namespace CreditExchange.EventDrivenNotifications.Api
{
    public class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            services.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddTenantTime();
            services.AddTokenHandler();
            services.AddHttpServiceLogging(Settings.ServiceName);
            services.AddEventHub(Settings.EventHub.Host, Settings.EventHub.Port, Settings.Nats, Settings.ServiceName);
            services.AddTenantService(Settings.Tenant.Host, Settings.Tenant.Port);
            services.AddLookupService(Settings.LookupService.Host, Settings.LookupService.Port);
            services.AddDataAttributes(Settings.DataAttributes.Host, Settings.DataAttributes.Port);
            services.AddDecisionEngine(Settings.DecisionEngine.Host, Settings.DecisionEngine.Port);
            services.AddConfigurationService<EventDrivenNotificationConfiguration>(
                Settings.Configuration.Host,
                Settings.Configuration.Port,
                Settings.ServiceName);
            services.AddEmailService(Settings.Email.Host, Settings.Email.Port);
            services.AddSmsService(Settings.Sms.Host, Settings.Sms.Port);

            services.AddTransient<IEventDrivenNotificationServiceFactory, EventDrivenNotificationServiceFactory>();
            services.AddTransient<IEventDrivenNotificationService, EventDrivenNotificationService>();
            services.AddTransient<INotificationEventsListener, NotificationEventsListener>();
            services.AddMvc().AddLendFoundryJsonOptions();

            services.AddCors();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseCors(env);
            app.UseErrorHandling();
            try
            {
                app.UseNotificationEventsListener();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            
            app.UseMvc();
            app.UseHealthCheck();
        }
    }
}